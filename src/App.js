import React from 'react';
import './App.css';
import axios from 'axios';

class App extends React.Component {

  constructor() {
    super();
    this.state = {

    }
  }

  componentDidMount() {
    axios.get('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml')
      .then(response => {
        console.log(response);
      })
  }

  render() {
    return (
      <div className="container-fluid App">
        <div className='row'>
          <div className='col-sm-12 center padding-top'>
            <h2>Currency Converter</h2>
          </div>
        </div>
        <div className='row'>
          <div className='col-sm-3 offset-sm-3 padding-top'>

            <label>1 AUD = 0.8 USD</label>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text">$</span>
              </div>
              <input type="text" className="form-control" aria-label="Amount (to the nearest dollar)" />
              <div className="input-group-append">
                <span className="input-group-text">AUD</span>
              </div>
            </div>

          </div>
          <div className='col-sm-3 padding-top'>
            <label>1 USD = 1.2 AUD</label>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text">$</span>
              </div>
              <input type="text" className="form-control" aria-label="Amount (to the nearest dollar)" />
              <div className="input-group-append">
                <span className="input-group-text">USD</span>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default App;
